
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TvaKnappar_Ranta_RakAmortering extends JFrame implements ActionListener {
	
	protected JTextField kapital = new JTextField(15);
	protected JTextField antalAr = new JTextField(15);
	private JTextField ranta = new JTextField(15);
	
	private JLabel kap = new JLabel ("Startkapital / l�nm�ngd: ", JLabel.RIGHT);
	private JLabel ar = new JLabel ("Antal �r: ", JLabel.RIGHT);
	private JLabel proc = new JLabel ("R�nteprocent: ", JLabel.RIGHT);
	
	private JPanel p = new JPanel();
	private JLabel resultat = new JLabel();
	private JButton btn = new JButton("r�nta");
	private JButton btn2 = new JButton("rak amortering");
	
	public TvaKnappar_Ranta_RakAmortering () {
		setLayout(new GridLayout(4,1)); // fyra rader, en kolumn
		add(p);
		add(resultat);
		add(btn);
		add(btn2);
		
		// ==== ytterligare "JPanel" uppdelning
		p.setLayout(new GridLayout(3,2)); // inne i �vre halvan (JPANEL som kallades p) en ny grid 3 rader 2 kolumner
		p.add(kap); // l�gger in fr�gan p� rad 1 kol 1
		p.add(kapital); // l�gger in textrutan rad 1 kol 2
		p.add(ar); // rad 2 kol 1
		p.add(antalAr); // rad 2 kol 2
		p.add(proc); // rad 3 kol 1
		p.add(ranta); // rad 3 kol 2
		
		// kopplar ihop textrutan med "fr�gan"
		kap.setLabelFor(kapital);
		kap.setDisplayedMnemonic('S'); // S blir understr�ckat och genom <alt s> hoppar kursorn hit --- prova �ndra
		ar.setLabelFor(antalAr);
		ar.setDisplayedMnemonic('A'); // A blir understr�ckat och genom <alt s> hoppar kursorn hit
		proc.setLabelFor(ranta);
		proc.setDisplayedMnemonic('R'); // R blir understr�ckat och genom <alt s> hoppar kursorn hit
		
		resultat.setHorizontalAlignment(JLabel.CENTER);
		btn.addActionListener(this);
		btn2.addActionListener(new Amortering (this));
		// alternativ btn2.addActionListener(new Amortering (this, kapital.getText(), ranta.getText(), ar.getText(), resultat));
		pack();
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	protected double getRanta() { // m�jligg�r att ranta �r private
		double x = Double.parseDouble(ranta.getText());
		return x;
	}

	protected void setResultat(String u) { // m�jligg�r att ranta �r private
		resultat.setText(u);
	}

	// Actionlistener
public void actionPerformed (ActionEvent e) {
	String start = kapital.getText();
	double st = Double.parseDouble(start);
	
	String tid = antalAr.getText();
	double t = Double.parseDouble(tid);
	
	String ranteprocent = ranta.getText();
	double rp = Double.parseDouble(ranteprocent);
	rp = rp / 100 + 1;
	
	// ber�kning och printning
	double totKap = st * Math.pow(rp, t);
	String ut = "Med startkapital " + start + " har du efter " + tid + " med "
			+ "" + ranteprocent + " ranta ett slutkapital p� " + totKap;
	resultat.setText(ut);
}

public static void main (String[] args) {
	TvaKnappar_Ranta_RakAmortering f = new TvaKnappar_Ranta_RakAmortering();
}

	
}

class Amortering implements ActionListener {
	TvaKnappar_Ranta_RakAmortering w;
	 
	public Amortering(TvaKnappar_Ranta_RakAmortering f) {
		w = f;
	}

	public void actionPerformed(ActionEvent e) {
		double x = Double.parseDouble(w.kapital.getText()) / Double.parseDouble(w.antalAr.getText()); 
		w.setResultat("Du amorterar totalt " + x + " euro per m�nad, resten �r r�nta");
	}
}


