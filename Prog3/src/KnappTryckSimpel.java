
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class KnappTryckSimpel extends JFrame implements ActionListener {
	// instansvariabler
	private JPanel p = new JPanel();
	private JPanel p2 = new JPanel();
	private JLabel resultat = new JLabel();
	private JButton btn = new JButton("Texten i knappen");
	private int antalTryck = 0;
	
	public KnappTryckSimpel () { // toma konstruktorn
		setLayout(new GridLayout(2,1)); // tre rader, en kolumn
		add(p);
		add(p2);
		p.add(resultat);
		p2.add(btn);
	
		resultat.setHorizontalAlignment(JLabel.CENTER);
		btn.addActionListener(this);
		pack();
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}

	
	// Actionlistener
public void actionPerformed (ActionEvent e) {
	System.out.println(e.getActionCommand());
	antalTryck++;
	String ut = "Du har tryckt p� knappen " + antalTryck + " g�nger"; 
	resultat.setText(ut);
	pack();
}

public static void main (String[] args) throws IOException {
	KnappTryckSimpel r = new KnappTryckSimpel();
	}
}
