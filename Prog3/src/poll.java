import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

class poll extends JPanel implements ActionListener {
	  public poll( String question, 
	               String[] choices ) {
	    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			
	    setBorder(new TitledBorder(question));
			
	    ButtonGroup g = new ButtonGroup();
	    for (String c : choices) {
	      JRadioButton b = new JRadioButton(c);
	      add(b);
	      b.addActionListener(this);
	      g.add(b);
	    }
		setVisible(true);
		
	  }

	public void actionPerformed(ActionEvent w) {
		System.out.println("Valde en knapp");
		
	}

	}
