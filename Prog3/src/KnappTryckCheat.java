import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class KnappTryckCheat extends JFrame implements ActionListener {
	// instansvariabler
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	
	private JLabel resultat = new JLabel();
	private JButton btn = new JButton("Texten i knappen");
	private int antalTryck = 0;
	
	public KnappTryckCheat () { // toma konstruktorn
		setLayout(new GridLayout(3,1)); // tre rader, en kolumn
		add(p1);
		add(p2);
		add(p3);
		p2.add(resultat);
		p3.add(btn);
	
		resultat.setHorizontalAlignment(JLabel.CENTER);
		btn.addActionListener(this);
		pack();
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}


	KnappTryckCheat (String i) {
		// en annan konstruktor
	}
	
	// en setter bara f�r att visa
	void setAntalTryck (int i) {
		antalTryck = antalTryck + i;
		resultat.setText("Du har tryckt p� knappen " + antalTryck + " g�nger");
	}
		
	// Actionlistener
	public void actionPerformed (ActionEvent e) {
		antalTryck++;
		String ut = "Du har tryckt p� knappen " + antalTryck + " g�nger"; 
		resultat.setText(ut);
		pack();
	}

	public static void main (String[] args) throws IOException {
		KnappTryckCheat r = new KnappTryckCheat();
		while (true) {
			BufferedReader input = new BufferedReader (new InputStreamReader(System.in));
			System.out.println("Cheat: ange hur m�nga knapptryck till: ");
			System.out.println(r.antalTryck);
			String svar = input.readLine();
			int n = Integer.parseInt(svar);
			r.setAntalTryck(n);
		}
	
		
	}
}
