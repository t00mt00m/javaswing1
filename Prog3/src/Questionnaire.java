import javax.swing.BoxLayout;
import javax.swing.JFrame;

public class Questionnaire extends JFrame {
	public poll w;
	public Questionnaire() {
		setTitle("Favorites");
		
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		
		String[] c1 = { "Paris", "London", "Milan", "La Paz" };
		w = new poll("City:", c1);
		add(w);
		
		String[] c2 = { "Italian", "Mexican", "Indian" };
		add(new poll("Food:", c2));
		
		String[] c3 = { "French", "Spanish", "Italian" };
		add(new poll("Wine:",c3));
		
		pack();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		Questionnaire p = new Questionnaire();

	}

}
